﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TShockAPI;

namespace BeanShop
{
    public static class ConfigUtils
    {
        public static ShopConfig config=new ShopConfig();
        public static string dirPath = TShock.SavePath + "/BeanShop";
        public static string configPath = dirPath + "/config.json";
        public static string shopsPath = dirPath + "/shops.json";
        public static void InitializeConfig() {
            if (Directory.Exists(dirPath))
            {
                if (File.Exists(configPath))
                {
                    config = JsonConvert.DeserializeObject<ShopConfig>(File.ReadAllText(configPath));
                }
                else
                {
                    File.WriteAllText(configPath, JsonConvert.SerializeObject(config, Formatting.Indented));
                }
                if (File.Exists(shopsPath))
                {
                    Main.shops = JsonConvert.DeserializeObject<List<ShopChest>>(File.ReadAllText(shopsPath));
                }
                else
                {
                    File.WriteAllText(shopsPath, JsonConvert.SerializeObject(Main.shops, Formatting.Indented));
                }
            }
            else
            {
                Directory.CreateDirectory(dirPath);
                File.WriteAllText(configPath, JsonConvert.SerializeObject(config, Formatting.Indented));
                File.WriteAllText(shopsPath, JsonConvert.SerializeObject(Main.shops, Formatting.Indented));
            }
        }
        public static void ReloadConfig() {
            Main.shops = JsonConvert.DeserializeObject<List<ShopChest>>(File.ReadAllText(shopsPath));
            config = JsonConvert.DeserializeObject<ShopConfig>(File.ReadAllText(configPath));
        }
        public static void UpdateShops() {
            File.WriteAllText(shopsPath, JsonConvert.SerializeObject(Main.shops, Formatting.Indented));
        }
        public class ShopConfig {
            public int OpenRequireMoney { get; set; }
            public int MaxOwnCount { get; set; }
            public ShopConfig()
            {
                OpenRequireMoney = 100;
                MaxOwnCount = 10;
            }

        }
    }
}

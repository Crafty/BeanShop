﻿using BeanPoints;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.Localization;
using TShockAPI;

namespace BeanShop
{
    public class ShopChest
    {
        public int ID { get; set; }
        public int GoodsID { get; set; }
        public int GoodPrice { get; set; }
        public int RealChestID { get; set; }
        [JsonIgnore]
        public Chest RealChest { get; set; }
        public ShopType Type { get; set; }
        public int TileX { get; set; }
        public int TileY { get; set; }
        public string Owner { get; set; }
        [JsonIgnore]
        public bool IsSelected { get; set; }
        [JsonIgnore]
        public bool IsFull { get; set; }
        public ShopChest(int ID, string Name, ShopType Type, int TileX, int TileY, string Owner,int Price) {
            this.ID = ID;
            this.Type = Type;
            this.TileX = TileX;
            this.TileY = TileY;
            this.Owner = Owner;
            this.GoodsID = 0;
            this.GoodPrice = Price;
            this.RealChestID = -1;
            this.RealChest = null;
            this.IsSelected = false;
        }
        public ShopChest() {
        }
     
        public void SetBuyType() {
            this.Type = ShopType.Buy;

        }
     
        public void SetSellType()
        {
            this.Type = ShopType.Sell;
        }
      
        public void SetCommon() {
            this.Type = ShopType.Common;
        }
        public void SetGoodItem(int id) {
            this.GoodsID = id;
        }
        //to do
        public string SetChestName() {
            string name= "";
            if (RealChestID == -1)
                return name;
            if (Type!=ShopType.Common)
            {
                name = $"[[c/3CB371:{Type}]][i:{GoodsID}]";
            }
            return name;
        }   
        public static int GetMaxShopID(List<ShopChest> shops)
        {
            int maxNum = 0;
            if (shops.Count != 0)
            {
                for (int i = 0; i < shops.Count; i++)
                {
                    if (maxNum < shops[i].ID)
                    {
                        maxNum = shops[i].ID;
                    }
                }
            }
            return maxNum;
        }
        public bool UpdateChest() {
            for (int i = 0; i < RealChest.item.Length; i++)
            {
                var item = RealChest.item[i];
                TSPlayer.All.SendData(PacketTypes.ChestItem,"",RealChestID,i);
            }
            return true;
        }
        public int GetTotal() {
            int total=0;
            if ((RealChestID!=-1||RealChestID!=0)&&RealChest!=null&&GoodsID!=0)
            {
                foreach (var item in RealChest.item)
                {
                    if (item.netID==GoodsID)
                    {
                        total += item.stack;
                    }
                }
            }
            return total;
        }
        public string ShowShopInfo() {
            var sb = new StringBuilder();
            switch (this.Type)
            {
                case ShopType.Sell:
                    sb.AppendLine("——商店信息——");
                    sb.AppendLine($"店主:{Owner}");
                    sb.AppendLine($"商店类型:{Type.ToString().Replace("Sell","出售")}");
                    sb.AppendLine($"出售商品:[i:{GoodsID}]");
                    sb.AppendLine($"商店库存:{GetTotal()}");
                    sb.AppendLine($"商品单价:{GoodPrice}");
                    sb.AppendLine("————————");
                    break;
                case ShopType.Buy:
                    sb.AppendLine("——商店信息——");
                    sb.AppendLine($"店主:{Owner}");
                    sb.AppendLine($"商店类型:{Type.ToString().Replace("Buy", "收购")}");
                    sb.AppendLine($"收购商品:[i:{GoodsID}]");
                    sb.AppendLine($"商店库存:{GetTotal()}");
                    sb.AppendLine($"商品单价:{GoodPrice}");
                    sb.AppendLine("————————");
                    break;
                case ShopType.Common:
                    sb.AppendLine("该商店未设置好相关信息");
                    break;
            }
            return sb.ToString();
        }
        public void IntoChest(Item item) {
            int slot = 0;
            for (int i = 0; i < RealChest.item.Length; i++)
            {
                var _item = RealChest.item[i];
                if (_item.netID == 0)
                {
                    _item.netDefaults(item.netID);
                    _item.stack = item.stack;
                    _item.prefix = item.prefix;
                    TSPlayer.All.SendData(PacketTypes.ChestItem, "", RealChestID, i);
                    break;
                }
                else if (_item.stack < 99)
                {
                    _item.netDefaults(item.netID);
                    _item.stack += item.stack;
                    _item.prefix = item.prefix;
                    TSPlayer.All.SendData(PacketTypes.ChestItem, "", RealChestID, i);
                    break;
                }
                else {
                    slot++;
                }
                if (slot == 40)
                {
                    IsFull = true;
                }
            }
        }
        public bool OutChest(TSPlayer plr,int aimcount)
        {
            if (RealChest == null || RealChestID == -1)
                return false;
            for (int i = 0; i < RealChest.item.Length; i++)
            {
                var item = RealChest.item[i];
                if (item.netID == 0)
                {
                    continue;
                }
                else if (item.stack < aimcount)
                {
                    aimcount -= item.stack;
                    plr.GiveItem(item.netID, item.stack, item.prefix);
                    item.stack = 0;
                    TSPlayer.All.SendData(PacketTypes.ChestItem, "", RealChestID, 0);
                }
                else if (item.stack >= aimcount) {
                    plr.GiveItem(item.netID, aimcount, item.prefix);
                    item.stack -=aimcount ;
                    aimcount = 0;
                    TSPlayer.All.SendData(PacketTypes.ChestItem, "", RealChestID, 0);
                }
                if (aimcount==0)
                {
                    break;
                }
            }
            return true;
        }
        public bool Open(int plrid,ShopChest chest) {
            if (RealChestID==-1&&RealChest==null)
            {
                return false;
            }
            if (UpdateChest())
            {
                return true;
            }
            return false;
        }
        public BeanPlayer ReturnOwnerAccount() {
            return BeanPlayer.GetBeanPlayer(Owner);
        }
        public BeanPlayer ReturnOwnerAccountNoTP()
        {
            return BeanPlayer.GetBeanPlayerNoTP(Owner);
        }
        public string ReturnType() {
            switch (Type)
            {
                case ShopType.Sell:
                    return "出售";
                case ShopType.Buy:
                    return "收购";
                case ShopType.Common:
                    return "普通";
                default:
                    return "";
            }
        }
    }
    public enum ShopType { 
    Sell,
    Buy,
    Common
    }
}

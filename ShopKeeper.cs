﻿using BeanPoints;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TShockAPI;

namespace BeanShop
{
    public class ShopKeeper
    {
        public TSPlayer Player { get; set; }

        public List<ShopChest> Chests = new List<ShopChest>();
        public KeeperStatus Status { get; set; }
        public int CurrentShopID { get; set; }
        public ShopKeeper(TSPlayer tsplr, KeeperStatus status, int currentShopID) {
            Player = tsplr;
            Status = status;
            CurrentShopID = currentShopID;
        }
        public ShopKeeper() { }
        /// <summary>
        /// 将玩家状态设置为-创建商店
        /// </summary>
        public void SetCreateShopType() {
            Status = KeeperStatus.CreateShop;
        }
        /// <summary>
        /// 将玩家状态设置为-上架物品
        /// </summary>
        public void SetUpitemType()
        {
            Status = KeeperStatus.UpItem;
        }
        /// <summary>
        /// 将玩家状态设置为-移除商店
        /// </summary>
        public void SetRemoveShopType()
        {
            Status = KeeperStatus.RemoveShop;
        }
        /// <summary>
        /// 将玩家状态设置为-设置购买
        /// </summary>
        public void SetBuyType()
        {
            Status = KeeperStatus.SetBuy;
        }
        /// <summary>
        /// 将玩家状态设置为-设置出售
        /// </summary>
        public void SetSellType()
        {
            Status = KeeperStatus.SetSell;
        }
        /// <summary>
        /// 将玩家状态设置为-设置收购数量
        /// </summary>
        public void SetLimit()
        {
            Status = KeeperStatus.SetLimit;
        }
        /// <summary>
        /// 将玩家状态设置为-设置商品价格
        /// </summary>
        public void SetPrice()
        {
            Status = KeeperStatus.SetPrice;
        }
        /// <summary>
        /// 将玩家状态设置为-购买
        /// </summary>
        /// <param name="chestid"></param>
        public void BuyType(int chestid)
        {
            CurrentShopID = chestid;
            Status = KeeperStatus.Buy;
        }
        /// <summary>
        /// 将玩家状态设置为-无操作状态
        /// </summary>
        public void SetCommonType()
        {
            Status = KeeperStatus.Common;
            CurrentShopID = 0;
        }
        /// <summary>
        /// 判断玩家是否为该商店主人
        /// </summary>
        /// <param name="chest"></param>
        /// <returns></returns>
        public bool IsOwner(ShopChest chest) {
            if (chest.Owner == Player.Name)
            {
                return true;
            }
            else {
                return false;
            }
        }
        public void SendMessage(string msg,Color color) {
            Player.SendMessage(msg,color);
        }
        public void SendInfoMessage(string msg) {
            SendMessage(msg,Color.SpringGreen);
        }
        public void SendErrorMessage(string msg)
        {
            SendMessage(msg, Color.Crimson);
        }
        public void SendSuccessMessage(string msg)
        {
            SendMessage(msg,Color.DarkOrange);
        }
        public void SetCurrentShopID(int id) {
            CurrentShopID = id;
        }
        public ShopChest GetCurrentShop() {
            if (CurrentShopID != 0)
            {
                return Main.shops.Find(s => s.ID == CurrentShopID);
            }
            else {
                return null;
            }
        }
        public void ClearTrashItem() {
            Player.TPlayer.trashItem.stack = 0;
            Player.SendData(PacketTypes.PlayerSlot,Player.TPlayer.trashItem.Name,Player.Index, 179, Player.TPlayer.trashItem.prefix);
        }
    }
    public enum KeeperStatus { 
    Common,
    CreateShop,
    UpItem,
    RemoveShop,
    SetPrice,
    SetBuy,
    SetSell,
    Buy,
    Sell,
    SetLimit,
    SetBuyItem,
    Debug
    }
}
